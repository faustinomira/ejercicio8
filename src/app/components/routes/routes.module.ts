import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { ContactoComponent } from './contacto/contacto.component';
import { GaleriaComponent } from './galeria/galeria.component';



@NgModule({
  declarations: [
    InicioComponent,
    ContactoComponent,
    GaleriaComponent,
  ],
  imports: [
    CommonModule
  ]
})
export class RoutesModule { }
